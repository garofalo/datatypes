package org.dame.vogc.datatypes;

/**
 *
 * @author Sabrina Checola
 */
public class PlsAttribute {

    private int id;
    private String name;
    private String description;
    private String ucd;
    private String datatype;
    private boolean primaryAtt;

    /**
     * Constructs a generic pulsar attribute object
     */
    public PlsAttribute() {
    }

    /**
     * Constructs a pulsar attribute with a specified id, name, description,
     * ucd, datatype and primary attribute.
     *
     * @param id unique integer that identify a star attribute
     * @param name string that specify the attribute name
     * @param description string that specify the description of the attribute
     * @param ucd string that specify the ucd of the attribute
     * @param datatype string that specify the datatype of the attribute
     * @param primaryAtt boolean that specify if the attribute id primary or not
     */
    public PlsAttribute(int id, String name, String description, String ucd, String datatype, boolean primaryAtt) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.ucd = ucd;
        this.datatype = datatype;
        this.primaryAtt = primaryAtt;
    }

    public String getDatatype() {
        return datatype;
    }

    public String getDescription() {
        return description;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isPrimaryAtt() {
        return primaryAtt;
    }

    public String getUcd() {
        return ucd;
    }

    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrimaryAtt(boolean primaryAtt) {
        this.primaryAtt = primaryAtt;
    }

    public void setUcd(String ucd) {
        this.ucd = ucd;
    }
}
