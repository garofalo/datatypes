package org.dame.vogc.datatypes;

/**
 *
 * @author Sabrina Checola
 */
public class Star extends VObject {

    private static final int objectType = 3;
    private String clusterId;

    public Star() {
        super(objectType);
    }

    /**
     * Constructs a star with a specified id.
     *
     * @param starId unique string that identify a star
     */
    public Star(String starId, String clusterId) {
        super(starId, objectType);
        this.clusterId = clusterId;
    }

    /**
     * Gets the cluster in which the star is contained
     *
     * @return cluster - cluster object containing the star
     */
    public String getGclusterId() {
        return clusterId;
    }

    public void setGclusterId(String clusterId) {
        this.clusterId = clusterId;
    }
}
