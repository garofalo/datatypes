package org.dame.vogc.datatypes;

import java.util.Date;

/**
 *
 * @author Luca
 */
public class GclusterHasAttribute {

    private int id;
    private int gcAttId;
    private String gclusterId;
    private String sourceId;
    private String value;
    private Date date;
    private boolean first;

    /**
     * Constructs a GclusterHasAttribute object with a specified sourceId and
     * globular cluster Id
     *
     * @param gclusterId
     * @param sourceId
     */
    public GclusterHasAttribute() {
    }

    /**
     * Constructs a GclusterHasAttribute object with a specified id, globular
     * cluster attribute id, source id, a value, date of insert and a boolean
     * flag.
     *
     * @param id unique integer that identify a GclusterHasAttribute object
     * @param gcAttId unique integer that identify a globular cluster attribute
     * @param gclusterId unique string that identify a globular cluster
     * @param sourceId unique string that identify a source
     * @param value string containing the value of an attribute
     * @param date date of the insert
     * @param first boolean flag for internal controlls
     */
    public GclusterHasAttribute(int id, String gclusterId, int gcAttId, String sourceId, String value, boolean first, Date date) {

        this.id = id;
        this.gcAttId = gcAttId;
        this.gclusterId = gclusterId;
        this.sourceId = sourceId;
        this.value = value;
        this.date = date;
        this.first = first;
    }

    /**
     * Gets the date of the insert
     *
     * @return - date object contaning the date of insert
     */
    public Date getDate() {
        return date;
    }

    /**
     * Gets the value of the boolean flag
     *
     * @return - a boolean value for internal controlls
     */
    public boolean isFirst() {
        return first;
    }

    /**
     * Gets the GclusterHasAttribute object id
     *
     * @return - unique integer that identify a GclusterHasAttribute object
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the source id
     *
     * @return - unique string that identify a source
     */
    public String getSourceId() {
        return sourceId;
    }

    /**
     * Gets the globular cluster attribute id
     *
     * @return - unique integer that identify a globular cluster attribut
     */
    public int getGcAttId() {
        return gcAttId;
    }

    /**
     * Gets the value of the attribute
     *
     * @return - string containing the value of an attribute
     */
    public String getValue() {
        return value;
    }

    /**
     * Gets the value of the globular cluster id
     *
     * @return - string containing the value of a globular cluster id
     */
    public String getGclusterId() {
        return gclusterId;
    }

    /**
     * Sets the value of the globular cluster id
     *
     * @param gclusterId string containing the value of a globular cluster id
     */
    public void setGclusterId(String gclusterId) {
        this.gclusterId = gclusterId;
    }

    /**
     * Sets the date of the insert
     *
     * @param date date object contaning the date of insert
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Sets the value of the boolean flag
     *
     * @param first a boolean value for internal controlls
     */
    public void setFirst(boolean first) {
        this.first = first;
    }

    /**
     * Sets the GclusterHasAttribute object id
     *
     * @param id unique integer that identify a GclusterHasAttribute object
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Sets the source id
     *
     * @param sourceId unique string that identify a source
     */
    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    /**
     * Sets the globular cluster attribute id
     *
     * @param gcAttId unique integer that identify a globular cluster attribut
     */
    public void setGcAttId(int gcAttId) {
        this.gcAttId = gcAttId;
    }

    /**
     * Sets the value of the attribute
     *
     * @return value string containing the value of an attribute
     */
    public void setValue(String value) {
        this.value = value;
    }
}
