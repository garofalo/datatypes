package org.dame.vogc.datatypes;

import java.util.Date;

/**
 *
 * @author Sabrina Checola
 */
public class Session {

    private int ssid;
    private String userId;
    private Date creationDate;
    private Date lastAccessDate;

    /**
     * Constructs a Session object with a specific ssid, userId, creation date
     * and last access date
     *
     * @param ssid unique integer that identify a session
     * @param userId unique string that identify the user who owns the session
     * @param creationDate session's creation date
     * @param lastAccessDate user's last access date
     */
    public Session(int ssid, String userId, Date creationDate, Date lastAccessDate) {
        this.ssid = ssid;
        this.userId = userId;
        this.creationDate = creationDate;
        this.lastAccessDate = lastAccessDate;
    }

    /**
     * Gets the session's creation date
     *
     * @return - a date object which contains the session's creation date
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * Gets the user's last access date
     *
     * @return - a date object which contains the user's last access date
     */
    public Date getLastAccessDate() {
        return lastAccessDate;
    }

    /**
     * Gets the session id
     *
     * @return - unique integer that identify a session
     */
    public int getSsid() {
        return ssid;
    }

    /**
     * Gets the user id
     *
     * @return - unique string that identify the user who owns the session
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the session's creation date
     *
     * @param creationDate date object which contains the session's creation
     * date
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Sets the user's last access date
     *
     * @param lastAccessDate date object which contains the user's last access
     * date
     */
    public void setLastAccessDate(Date lastAccessDate) {
        this.lastAccessDate = lastAccessDate;
    }

    /**
     * Sets the session id
     *
     * @param ssid unique integer that identify a session
     */
    public void setSsid(int ssid) {
        this.ssid = ssid;
    }

    /**
     * Sets the user id
     *
     * @param userId unique string that identify the user who owns the session
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }
}
