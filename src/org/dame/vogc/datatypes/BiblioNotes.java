package org.dame.vogc.datatypes;

import java.util.Date;

/**
 *
 * @author Luca
 */
public class BiblioNotes {

    private int id;
    private String objectId;
    private int gcAttributeId;
    private int plsAttributeId;
    private int starAttributeId;
    private String userId;
    private int imageId;
    private Date date;
    private int type;

    /**
     * Constructs a BiblioNotes object with specified id, object id, globular
     * cluster attribute id, pulsar attribute id, star attribute id, user id,
     * image id, date of insert and type of bibliographic note
     *
     * @param id unique integer that identify a BiblioNotes object
     * @param objectId unique string that identify an object
     * @param gcAttributeId unique integer that identify a globular cluster's
     * attribute
     * @param plsAttributeId unique integer that identify a pulsar's attribute
     * @param starAttributeId unique integer that identify a star's attribute
     * @param userId unique string that identify a user object
     * @param imageId unique integer that identify a image object
     * @param date date object that specify the date of insert
     * @param type integer that specify the type of biblio notes: 1 for object
     * Notes, 2 for object BiblioRef
     */
    public BiblioNotes(int id, String objectId, int gcAttributeId, int starAttributeId, int plsAttributeId, String userId, int imageId, Date date, int type) {

        this.id = id;
        this.objectId = objectId;
        this.gcAttributeId = gcAttributeId;
        this.plsAttributeId = plsAttributeId;
        this.starAttributeId = starAttributeId;
        this.userId = userId;
        this.imageId = imageId;
        this.date = date;
        this.type = type;
    }

    /**
     * Gets the date of insert
     *
     * @return - date object conteining the date of insert
     */
    public Date getDate() {
        return date;
    }

    /**
     * Gets the globular cluster attribute id
     *
     * @return - an integer that identify a globular cluster's attribute
     */
    public int getGcAttributeId() {
        return gcAttributeId;
    }

    /**
     * Gets the BiblioNote object id
     *
     * @return - a integer that identify a BiblioNotes object
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the image id
     *
     * @return - a integer that identify a image object
     */
    public int getImageId() {
        return imageId;
    }

    /**
     * Gets the object id
     *
     * @return - a string that identify an object
     */
    public String getObjectId() {
        return objectId;
    }

    /**
     * Gets the pulsar's attribute id
     *
     * @return - an integer that identify a pulsar's attribute
     */
    public int getPlsAttributeId() {
        return plsAttributeId;
    }

    /**
     * Gets the star's attribute id
     *
     * @return - a integer that identify a star's attribute
     */
    public int getStarAttributeId() {
        return starAttributeId;
    }

    /**
     * Gets the user id
     *
     * @return - a string that identify a user object
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Gets the BiblioNotes object's type
     *
     * @return - an integer that specify the type of biblio notes
     */
    public int getType() {
        return type;
    }

    /**
     * Sets the date of insert
     *
     * @param date object conteining the date of insert
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Sets the globular cluster attribute id
     *
     * @param an integer that identify a globular cluster's attribute
     */
    public void setGcAttributeId(int gcAttributeId) {
        this.gcAttributeId = gcAttributeId;
    }

    /**
     * Sets the BiblioNote object id
     *
     * @param an integer that identify a BiblioNotes object
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Sets the image id
     *
     * @param an integer that identify a image object
     */
    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    /**
     * Sets the object id
     *
     * @param a string that identify an object
     */
    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    /**
     * Sets the pulsar's attribute id
     *
     * @param an integer that identify a pulsar's attribute
     */
    public void setPlsAttributeId(int plsAttributeId) {
        this.plsAttributeId = plsAttributeId;
    }

    /**
     * Sets the star's attribute id
     *
     * @param an integer that identify a star's attribute
     */
    public void setStarAttributeId(int starAttributeId) {
        this.starAttributeId = starAttributeId;
    }

    /**
     * Sets the user id
     *
     * @param a string that identify a user object
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * Sets the BiblioNotes object's type
     *
     * @param an integer that specify the type of biblio notes
     */
    public void setType(int type) {
        this.type = type;
    }
}
