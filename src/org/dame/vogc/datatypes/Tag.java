package org.dame.vogc.datatypes;

/**
 *
 * @author Sabrina Checola
 */
public class Tag {

    private int id;
    private String name;

    /**
     * Constructs a Tag object with a specific specified id and name
     *
     * @param id unique integer that identify a Tag object
     * @param name string which contains the tag's value
     */
    public Tag(int id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Gets the tag id
     *
     * @return - integer that identify a Tag object
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the tag id
     *
     * @param id integer that identify a Tag object
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the tag name
     *
     * @return - string which contains the tag's value
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the tag name
     *
     * @param name string which contains the tag's value
     */
    public void setName(String name) {
        this.name = name;
    }
}
