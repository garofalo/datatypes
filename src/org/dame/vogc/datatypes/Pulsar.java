package org.dame.vogc.datatypes;

/**
 *
 * @author Sabrina Checola
 */
public class Pulsar extends VObject {

    private static final int objectType = 2;
    private String gclusterId;

    /**
     * Constructs a pulsar object
     */
    public Pulsar() {
        super(objectType);
    }

    /**
     * Constructs a pulsar.
     *
     * @param id unique string that identify an object
     * @param type integer that specify the type of the object
     */
    public Pulsar(String pulsarId, String gclusterId) {

        super(pulsarId, objectType);
        this.gclusterId = gclusterId;

    }

    /**
     *
     * @return cluster - return the cluster object that owns the Star
     */
    public String getGclusterId() {
        return this.gclusterId;
    }

    public void setGclusterId(String gclusterId) {
        this.gclusterId = gclusterId;
    }
}
