package org.dame.vogc.datatypes;

import java.util.Date;

/**
 *
 * @author Sabrina Checola
 */
public class PulsarHasAttribute {

    private int id;
    private int plsAttId;
    private String plsId;
    private String sourceId;
    private String value;
    private Date date;
    private boolean first;

    /**
     * Constructs a generic PulsarHasAttribute object
     */
    public PulsarHasAttribute() {
    }

    /**
     * Constructs a PulsarHasAttribute object with a specified id, globular
     * cluster attribute id, source id, a value, date of insert and a boolean
     * flag.
     *
     * @param id unique integer that identify a PulsarHasAttribute object
     * @param plsAttId unique integer that identify a pulsar attribute
     * @param plsId unique integer that identify a pulsar
     * @param sourceId unique string that identify a source
     * @param value string containing the value of an attribute
     * @param date date of the insert
     * @param first boolean flag for internal controlls
     */
    public PulsarHasAttribute(int id, int plsAttId, String plsId, String sourceId, String value, Date date, boolean first) {
        this.id = id;
        this.plsAttId = plsAttId;
        this.plsId = plsId;
        this.sourceId = sourceId;
        this.value = value;
        this.date = date;
        this.first = first;
    }

    /**
     * Gets the date of the insert
     *
     * @return - date object contaning the date of insert
     */
    public Date getDate() {
        return date;
    }

    /**
     * Gets the value of the boolean flag
     *
     * @return - a boolean value for internal controlls
     */
    public boolean isFirst() {
        return first;
    }

    /**
     * Gets the PulsarHasAttribute object id
     *
     * @return - unique integer that identify a PulsarHasAttribute object
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the source id
     *
     * @return - unique string that identify a source
     */
    public String getSourceId() {
        return sourceId;
    }

    /**
     * Gets the pulsar attribute id
     *
     * @return - unique integer that identify a pulsar attribute
     */
    public int getPlsAttId() {
        return plsAttId;
    }

    /**
     * Gets the value of the attribute
     *
     * @return - string containing the value of an attribute
     */
    public String getValue() {
        return value;
    }

    /**
     * Gets the pulsar id
     *
     * @return - string containing the pulsar id
     */
    public String getPlsId() {
        return plsId;
    }

    /**
     * Sets the pulsar id
     *
     * @param pulsarId string containing the pulsar id
     */
    public void setPlsId(String plsId) {
        this.plsId = plsId;
    }

    /**
     * Sets the date of the insert
     *
     * @param date date object contaning the date of insert
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Sets the value of the boolean flag
     *
     * @param first a boolean value for internal controlls
     */
    public void setFirst(boolean first) {
        this.first = first;
    }

    /**
     * Sets the GclusterHasAttribute object id
     *
     * @param id unique integer that identify a GclusterHasAttribute object
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Sets the source id
     *
     * @param sourceId unique string that identify a source
     */
    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    /**
     * Sets the pulsar attribute id
     *
     * @param gcAttId unique integer that identify a pulsar attribute
     */
    public void setPlsAttId(int plsAttId) {
        this.plsAttId = plsAttId;
    }

    /**
     * Sets the value of the attribute
     *
     * @return value string containing the value of an attribute
     */
    public void setValue(String value) {
        this.value = value;
    }
}
