package org.dame.vogc.datatypes;

/**
 *
 * @author Sabrina Checola
 */
public class Gcluster extends VObject {

    private static final int objectType = 1;
    private String name;
    private String clusterId;
    private int clusterType;

    /**
     * Constructs a globular cluster object
     *
     */
    public Gcluster() {
        super(objectType);
    }

    /**
     * Constructs a cluster with a specified id, name and type.
     *
     * @param clusterId string containing the cluster id
     * @param objectType integer containing the object type
     * @param clusterType integer containing the cluster type
     */
    public Gcluster(String clusterId, String name, int clusterType) {

        super(clusterId, objectType);
        this.clusterId = clusterId;
        this.clusterType = clusterType;
        this.name = name;
    }

    /**
     *
     * @return name - string containing the name of the cluster
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the cluster
     *
     * @param name string containing the name of the cluster
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the type of the cluster (galactic cluster or extragactic cluster)
     *
     * @return type - integer containing the type of the cluster
     */
    public int getClusterType() {
        return clusterType;
    }

    /**
     * Sets the type of the cluster (galactic cluster or extragactic cluster)
     *
     * @param type integer containing the type of the cluster
     */
    public void setClusterType(int clusterType) {
        this.clusterType = clusterType;
    }

    public String getClusterId() {
        return clusterId;
    }

    public void setClusterId(String clusterId) {
        this.clusterId = clusterId;
    }
}
