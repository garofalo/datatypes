package org.dame.vogc.datatypes;

/**
 *
 * @author Sabrina Checola
 */
public class Author {

    private int id;
    private String name;
    private String uri;

    /**
     * Constructs an Author object with a specific id, name and uri
     *
     * @param id unique integer that identify an Author object
     * @param name string that specify the author's name
     * @param uri string that specify an uri for informations about the author
     */
    public Author(int id, String name, String uri) {
        this.id = id;
        this.name = name;
        this.uri = uri;
    }

    /**
     * Gets the author's id
     *
     * @return - integer that identify an author
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the author's id
     *
     * @param id integer that identify an author
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the author's name
     *
     * @return - a string that specify the author's name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the author's name
     *
     * @param name string that specify the author's name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the author's uri
     *
     * @return - a string that specify an uri for informations about the author
     */
    public String getUri() {
        return uri;
    }

    /**
     * Sets the author's uri
     *
     * @param uri string that specify an uri for informations about the author
     */
    public void setUri(String uri) {
        this.uri = uri;
    }
}
