package org.dame.vogc.datatypes;

/**
 *
 * @author Sabrina Checola
 */
public class VObject {

    private String id;
    private int type;

    /**
     * Constructs a vobject object
     */
    public VObject() {
    }

    /**
     * Constructs a particular vobject object
     *
     * @param type specify the type of the object to construct
     */
    public VObject(int type) {
        this.type = type;
    }

    /**
     * Constructs an object with a specified id and type.
     *
     * @param id unique string that identify an object
     * @param type integer that specify the type of the object
     */
    public VObject(String id, int type) {
        this.id = id;
        this.type = type;
    }

    /**
     * Gets the object id
     *
     * @return id - string containing the object id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the object id
     *
     * @param id string containing the object id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets the object type
     *
     * @return type - integer containing the object type
     */
    public int getType() {
        return type;
    }

    /**
     * Sets the object type
     *
     * @param type contains an integer value between 0 and 3
     *
     */
    public void setType(int type) {
        this.type = type;
    }
}
