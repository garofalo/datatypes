package org.dame.vogc.datatypes;

import java.util.Date;

/**
 *
 * @author Sabrina Checola
 */
public class Catalogue extends Source {

    private static final int sourceType = 1;
    private String title;
    private String description;
    private String year;
    private String uri;
    private Date date;

    /**
     * Constructs a catalogue object with a specific id, title, description,
     * year of publication, uri and date of insert
     *
     * @param catalogueId unique string that identify a catalogue object
     * @param title string contaning the catalogue's title
     * @param description string containing the catalogue's description
     * @param year string contaning the catalogue's year of pubblication
     * @param uri string contaning a catalogue's uri
     * @param date date object contaning the catalogue's date of insert
     */
    public Catalogue(String catalogueId, String title, String description, String year, String uri, Date date) {
        super(catalogueId, sourceType);
        this.title = title;
        this.description = description;
        this.year = year;
        this.uri = uri;
        this.date = date;

    }

    /**
     * Gets the catalogue's date of insert
     *
     * @return - a date object contaning the catalogue's date of insert
     */
    public Date getDate() {
        return date;
    }

    /**
     * Gets the catalogue's description
     *
     * @return - a string containing the catalogue's description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the catalogue's title
     *
     * @return - a string contaning the catalogue's title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Gets the catalogue's uri
     *
     * @return - a string contaning a catalogue's uri
     */
    public String getUri() {
        return uri;
    }

    /**
     * Gets the catalogue's year of pubblication
     *
     * @return - a string contaning the catalogue's year of pubblication
     */
    public String getYear() {
        return year;
    }

    /**
     * Sets the catalogue's date of insert
     *
     * @param date a date object contaning the catalogue's date of insert
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Sets the catalogue's description
     *
     * @param description a string containing the catalogue's description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Sets the catalogue's title
     *
     * @param title a string contaning the catalogue's title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Sets the catalogue's uri
     *
     * @param uri a string contaning a catalogue's uri
     */
    public void setUri(String uri) {
        this.uri = uri;
    }

    /**
     * Sets the catalogue's year of pubblication
     *
     * @param year a string contaning the catalogue's year of pubblication
     */
    public void setYear(String year) {
        this.year = year;
    }
}
