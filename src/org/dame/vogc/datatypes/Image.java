package org.dame.vogc.datatypes;

import java.util.Date;

/**
 *
 * @author Sabrina Checola
 */
public class Image {

    private int id;
    private String objectId;
    private String userId;
    private String name;
    private String description;
    private String format;
    private String dimension;
    private String uri;
    private Date date;
    private String scale;
    private int type;

    /**
     * Constructs an Image object with a specific id, name, description, format,
     * dimension, uri, date of insert, scale and type of image (astronomical
     * photo or diagram)
     *
     * @param id unique integer that identify an Image object
     * @param name string that specify the image's name
     * @param description string that specify the image's description
     * @param format string that specify the image's format
     * @param dimension string that specify the image's dimension
     * @param uri string that specify the image's uri
     * @param date string that specify the image's date of insert
     * @param scale string that specify the image's scale
     * @param type integer that specify the type of image
     */
    public Image(int id, String objectId, String userId, String name, String description, String format, String dimension, String uri, Date date, String scale, int type) {

        this.id = id;
        this.objectId = objectId;
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.format = format;
        this.dimension = dimension;
        this.uri = uri;
        this.date = date;
        this.scale = scale;
        this.type = type;
    }

    /**
     * Gets the image's date of isert
     *
     * @return - a date object which contains the image's date of insert
     */
    public Date getDate() {
        return date;
    }

    /**
     * Sets the image's date of insert
     *
     * @param date a date object which contains the image's date of insert
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Gets the image's description
     *
     * @return - a string which contains the image's description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the image's description
     *
     * @param description a string which contains the image's description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the image's dimension
     *
     * @return - a string which contains the image's dimension
     */
    public String getDimension() {
        return dimension;
    }

    /**
     * Sets the image's dimension
     *
     * @param dimension a string which contains the image's dimension
     */
    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    /**
     * Gets the image's format
     *
     * @return - a string which specify the image's format
     */
    public String getFormat() {
        return format;
    }

    /**
     * Sets the image's format
     *
     * @param format a string which specify the image's format
     */
    public void setFormat(String format) {
        this.format = format;
    }

    /**
     * Gets the image's id
     *
     * @return - an integer which contains the image's unique id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the image's id
     *
     * @param id an integer which contains the image's unique id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the image's name
     *
     * @return - a stringr which contains the image's name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the image's name
     *
     * @param name a string which contains the image's unique id
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the image's scale
     *
     * @return - a string which contains the image's scale
     */
    public String getScale() {
        return scale;
    }

    /**
     * Sets the image's scale
     *
     * @param scale a string which contains the image's scale
     */
    public void setScale(String scale) {
        this.scale = scale;
    }

    /**
     * Gets the type of image
     *
     * @return - a string which specify the type of image
     */
    public int getType() {
        return type;
    }

    /**
     * Sets the type of image
     *
     * @param type a string which specify the type of image
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * Gets the image's uri
     *
     * @return - a string which contains the image's uri
     */
    public String getUri() {
        return uri;
    }

    /**
     * Sets the image's uri
     *
     * @param uri a string which specify the image's uri
     */
    public void setUri(String uri) {
        this.uri = uri;
    }

    /**
     * Gets the object id which owns the image
     *
     * @return - a string which contains the object's uri
     */
    public String getObjectId() {
        return objectId;
    }

    /**
     * Sets the object id which owns the image
     *
     * @param objectId a string which contains the object id
     */
    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    /**
     * Gets the user's id which inserted the image
     *
     * @return - a string which contains the user id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the user id which inserted the image
     *
     * @param usertId a string which contains the user id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }
}
