package org.dame.vogc.datatypes;

/**
 *
 * @author Sabrina Checola
 */
public class Paper {

    private int id;
    private String name;
    private String uri;

    /**
     * Constructs a Paper object with a specific id, name and uri
     *
     * @param id unique integer that identify a Paper object
     * @param name string that specify the paper's name
     * @param uri string that specify the paper's uri
     */
    public Paper(int id, String name, String uri) {
        this.id = id;
        this.name = name;
        this.uri = uri;
    }

    /**
     * Gets the paper's id
     *
     * @return - integer that identify a Paper object
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the paper's id
     *
     * @param id integer that identify a Paper object
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the paper's name
     *
     * @return - a string that specify the paper's name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the paper's name
     *
     * @param name string that specify the paper's name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the paper's uri
     *
     * @return - a string that specify the paper's uri
     */
    public String getUri() {
        return uri;
    }

    /**
     * Sets the paper's uri
     *
     * @param uri string that specify the paper's uri
     */
    public void setUri(String uri) {
        this.uri = uri;
    }
}
