package org.dame.vogc.datatypes;

import java.io.Serializable;

/**
 *
 * @author Sabrina Checola
 */
public class User extends Source implements Serializable {

    private static final int sourceType = 2;
    private static final long serialVersionUID = 1L;
    private String userId;
    private String password;
    private String name;
    private String surname;
    private String motivation;
    private String country;
    private boolean active;

    /**
     * Constructs a user object with a specific userId, password, name, surname,
     * motivation and active flag
     *
     * @param userId unique string that identify a user object
     * @param password string that specify the user's password
     * @param name string that specify the user's name
     * @param surname string that specify the user's surname
     * @param motivation string that specify the user's motivations
     * @param active string that specify the user active flag, used to indicate
     * id the user is active or not
     */
    public User(String userId, String password, String name, String surname, String motivation, String country, boolean active) {

        super(userId, sourceType);
        this.userId = super.getId();
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.motivation = motivation;
        this.country = country;
        this.active = active;
    }

    /**
     * Gets the value of the active flag
     *
     * @return - a boolean value: true if the user is active, false otherwise
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Gets the user's motivations
     *
     * @return - a string containing the user's motivation relative to the
     * abilitation request
     */
    public String getAffiliation() {
        return motivation;
    }

    /**
     * Gets the user's country
     *
     * @return - a string containing the user's country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Gets the user's name
     *
     * @return - a string containing the user's name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the user's password
     *
     * @return - a string contaning the user's password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Gets the user's surname
     *
     * @return - a string containing the user's surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets the active flag
     *
     * @param active a boolean containing true if the user is active, false
     * otherwise
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Sets the user's motivations
     *
     * @param motivation a string containing the user's motivation relative to
     * the abilitation request
     */
    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    /**
     * Sets the user's country
     *
     * @param motivation a string containing the user's country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Sets the user's name
     *
     * @param name a string containing the user's name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the user's password
     *
     * @param password a string containing the user's password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Sets the user's surname
     *
     * @param surname a string containing the user's surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }
}
