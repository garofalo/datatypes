package org.dame.vogc.datatypes;

import java.util.Date;

/**
 *
 * @author Sabrina Checola
 */
public class Notes extends BiblioNotes {

    private static final int biblioNotesType = 1;
    private String title;
    private String description;
    private int noteType;

    /**
     * Constructs a Notes object with a specific specified id, object id,
     * globular cluster attribute id, pulsar attribute id, sta attribute id,
     * user id, image id, date of insert and type of bibliografic note
     *
     * @param id unique integer that identify a Notes object
     * @param objectId unique string that identify an object
     * @param gcAttributeId unique integer that identify a globular cluster's
     * attribute
     * @param plsAttributeId unique integer that identify a pulsar's attribute
     * @param starAttributeId unique integer that identify a star's attribute
     * @param userId unique string that identify a user object
     * @param imageId unique integer that identify a image object
     * @param date date object that specify the date of insert
     * @param title string containing the note's title
     * @param description string containing the note's description
     * @param noteType integer containing the note's type: 1 for a textual note,
     * 2 for an url
     */
    public Notes(int id,
            String objectId,
            int gcAttributeId,
            int plsAttributeId,
            int starAttributeId,
            String userId,
            int imageId,
            Date date,
            String title,
            String description,
            int noteType) {

        super(id, objectId, gcAttributeId, starAttributeId, plsAttributeId, userId, imageId, date, biblioNotesType);

        this.title = title;
        this.description = description;
        this.noteType = noteType;

    }

    /**
     * Gets the note's description
     *
     * @return - a string containing the note's description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the note's type
     *
     * @return - an integer containing the note's type
     */
    public int getNoteType() {
        return noteType;
    }

    /**
     * Gets the note's title
     *
     * @return - a string containing the note's title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the note's description
     *
     * @param a string containing the note's description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Sets the note's type
     *
     * @param a string containing the note's type
     */
    public void setNoteType(int noteType) {
        this.noteType = noteType;
    }

    /**
     * Sets the note's title
     *
     * @param a string containing the note's title
     */
    public void setTitle(String title) {
        this.title = title;
    }
}
