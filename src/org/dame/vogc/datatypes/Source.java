package org.dame.vogc.datatypes;

/**
 *
 * @author Sabrina Checola
 */
public class Source {

    private String id;
    private int type;

    /**
     * Constructs a source object with a specified id and type
     *
     * @param id unique string that identify a source object
     * @param type integer that specify the type of the source (catalogue or
     * user)
     */
    public Source(String id, int type) {
        this.id = id;
        this.type = type;
    }

    /**
     * Gets the source id
     *
     * @return - a string containing the source id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the source id
     *
     * @param id a string containing the source id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets the source type
     *
     * @return - integer that specify the type of the source, 1 for catalogue
     * type and 2 for user type
     */
    public int getType() {
        return type;
    }

    /**
     * Sets the source type
     *
     * @param type integer that specify the type of the source, 1 for catalogue
     * type and 2 for user type
     */
    public void setType(int type) {
        this.type = type;
    }
}
