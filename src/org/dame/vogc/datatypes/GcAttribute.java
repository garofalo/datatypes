package org.dame.vogc.datatypes;

/**
 *
 * @author Sabrina Checola
 */
public class GcAttribute {

    private int id;
    private String name;
    private String description;
    private String ucd;
    private String datatype;
    private boolean primaryAtt;
    private int type;

    /**
     * Constructs a globular cluster's generic attribute
     */
    public GcAttribute() {
    }

    /**
     * Constructs a globular cluster attribute with a specified id, name,
     * description, ucd, datatype, primary attribute and type.
     *
     * @param id unique integer that identify a globular cluster attribute
     * @param name string that specify the attribute name
     * @param description string that specify the description of the attribute
     * @param ucd string that specify the ucd of the attribute
     * @param datatype string that specify the datatype of the attribute
     * @param primaryAtt boolean that specify if the attribute id primary or not
     * @param type integer that specify the type of the attribute: 1 for
     * positional, 2 for photometric, 3 for structural, 4 for dinamic 0 for
     * generic -1 for null attribute
     */
    public GcAttribute(int id, String name, String description, String ucd, String datatype, boolean primaryAtt, int type) {

        this.id = id;
        this.name = name;
        this.description = description;
        this.ucd = ucd;
        this.datatype = datatype;
        this.primaryAtt = primaryAtt;
        this.type = type;
    }

    /**
     * Gets the datatype of the attribute
     *
     * @return - string that specify the datatype of the attribute
     */
    public String getDatatype() {
        return datatype;
    }

    /**
     * Gets the description of the attribute
     *
     * @return - string that specify the datatype of the attribute
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the attribute id
     *
     * @return - unique integer that identify a globular cluster attribute
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the name of the attribute
     *
     * @return - string that specify the attribute name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the property that indicates if the attribute is primary or not
     *
     * @return - boolean, true if the attribute is primary false otherwise
     */
    public boolean isPrimaryAtt() {
        return primaryAtt;
    }

    /**
     * Gets the ucd of the attribute
     *
     * @return - string that specify the ucd of the attribute
     */
    public String getUcd() {
        return ucd;
    }

    /**
     * Gets the type of the attribute
     *
     * @return - integer containing a specific number which identify an
     * attribute type
     */
    public int getType() {
        return type;
    }

    /**
     * Sets the datatype of the attribute
     *
     * @param datatype string that specify the datatype of the attribute
     */
    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    /**
     * Sets the description of the attribute
     *
     * @param description string that specify the description of the attribute
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Sets the id of the attribute
     *
     * @param id unique integer that identify a globular cluster attribute
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Sets the name of the attribute
     *
     * @param name string that specify the attribute name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the property that indicates if the attribute is primary or not
     *
     * @param primaryAtt boolean, true if the attribute is primary false
     * otherwise
     */
    public void setPrimaryAtt(boolean primaryAtt) {
        this.primaryAtt = primaryAtt;
    }

    /**
     * Sets the ucd of the attribute
     *
     * @param ucd string that specify the ucd of the attribute
     */
    public void setUcd(String ucd) {
        this.ucd = ucd;
    }

    /**
     * Sets the type of the attribute
     *
     * @param type integer containing a specific number which identify an
     * attribute type
     */
    public void setType(int type) {
        this.type = type;
    }
}
