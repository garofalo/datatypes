package org.dame.vogc.datatypes;

import java.util.Date;

/**
 *
 * @author Sabrina Checola
 */
public class StarHasAttribute {

    private int id;
    private int starAttId;
    private String starId;
    private String sourceId;
    private String value;
    private Date date;
    private boolean first;

    /**
     * Constructs a generic StarHasAttribute object
     */
    public StarHasAttribute() {
    }

    /**
     * Constructs a StarHasAttribute object with a specified id, globular
     * cluster attribute id, source id, a value, date of insert and a boolean
     * flag.
     *
     * @param id unique integer that identify a StarHasAttribute object
     * @param starAttId unique integer that identify a star attribute
     * @param sourceId unique string that identify a source
     * @param starId string containing the star id
     * @param value string containing the value of an attribute
     * @param date date of the insert
     * @param first boolean flag for internal controlls
     */
    public StarHasAttribute(int id, int starAttId, String starId, String sourceId, String value, Date date, boolean first) {
        this.id = id;
        this.starAttId = starAttId;
        this.sourceId = sourceId;
        this.starId = starId;
        this.value = value;
        this.date = date;
        this.first = first;
    }

    /**
     * Gets the date of the insert
     *
     * @return - date object contaning the date of insert
     */
    public Date getDate() {
        return date;
    }

    /**
     * Gets the value of the boolean flag
     *
     * @return - a boolean value for internal controlls
     */
    public boolean isFirst() {
        return first;
    }

    /**
     * Gets the StarHasAttribute object id
     *
     * @return - unique integer that identify a StarHasAttribute object
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the source id
     *
     * @return - unique string that identify a source
     */
    public String getSourceId() {
        return sourceId;
    }

    /**
     * Gets the star attribute id
     *
     * @return - unique integer that identify a star attribute
     */
    public int getStarAttId() {
        return starAttId;
    }

    /**
     * Gets the value of the attribute
     *
     * @return - string containing the value of an attribute
     */
    public String getValue() {
        return value;
    }

    /**
     * Gets the star id
     *
     * @return - string containing the star id
     */
    public String getStarId() {
        return starId;
    }

    /**
     * Sets the star id
     *
     * @param starId string containing the star id
     */
    public void setStarId(String starId) {
        this.starId = starId;
    }

    /**
     * Sets the date of the insert
     *
     * @param date date object contaning the date of insert
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Sets the value of the boolean flag
     *
     * @param first a boolean value for internal controlls
     */
    public void setFirst(boolean first) {
        this.first = first;
    }

    /**
     * Sets the StarHasAttribute object id
     *
     * @param id unique integer that identify a StarHasAttribute object
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Sets the source id
     *
     * @param sourceId unique string that identify a source
     */
    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    /**
     * Sets the star attribute id
     *
     * @param gcAttId unique integer that identify a star attribute
     */
    public void setStarAttId(int starAttId) {
        this.starAttId = starAttId;
    }

    /**
     * Sets the value of the attribute
     *
     * @return value string containing the value of an attribute
     */
    public void setValue(String value) {
        this.value = value;
    }
}
