package org.dame.vogc.datatypes;

import java.util.Date;

/**
 *
 * @author Sabrina Checola
 */
public class BiblioRef extends BiblioNotes {

    private static final int biblioNotesType = 2;
    private int paperId;
    private String title;
    private String description;
    private String uri;
    private String year;

    /**
     * Constructs a BiblioRef object with a specific specified id, object id,
     * globular cluster attribute id, pulsar attribute id, star attribute id,
     * user id, image id, date of insert, uri, title, description and type of
     * bibliografic note
     *
     * @param id unique integer that identify a BiblioRef object
     * @param objectId unique string that identify an object
     * @param gcAttributeId unique integer that identify a globular cluster's
     * attribute
     * @param plsAttributeId unique integer that identify a pulsar's attribute
     * @param starAttributeId unique integer that identify a star's attribute
     * @param userId unique string that identify a user object
     * @param imageId unique integer that identify a image object
     * @param date date object that specify the date of insert
     * @param paperId unique integer that identify the assciated paper
     * @param title string containing the bibliographic note's title
     * @param description string containing the bibliographic note's descriptio
     * @param uri string containing the note's uri
     * @param year string containing the note's year of pubblication
     */
    public BiblioRef(int id,
            String objectId,
            int gcAttributeId,
            int plsAttributeId,
            int starAttributeId,
            String userId,
            int imageId,
            int paperId,
            Date date,
            String title,
            String description,
            String uri,
            String year) {

        super(id, objectId, gcAttributeId, starAttributeId, plsAttributeId, userId, imageId, date, biblioNotesType);

        this.paperId = paperId;
        this.title = title;
        this.description = description;
        this.uri = uri;
        this.year = year;
    }

    /**
     * Gets the note's description
     *
     * @return - a string containing the note's description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the note's description
     *
     * @param a string containing the note's description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the note's title
     *
     * @return - a string containing the note's title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the note's title
     *
     * @param a string containing the note's title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets the note's uri
     *
     * @return - a string containing the note's uri
     */
    public String getUri() {
        return uri;
    }

    /**
     * Sets the note's uri
     *
     * @param a string containing the note's uri
     */
    public void setUri(String uri) {
        this.uri = uri;
    }

    /**
     * Gets the note's year of pubblication
     *
     * @return - a string containing the year of pubblication
     */
    public String getYear() {
        return year;
    }

    /**
     * Sets the note's year of pubblication
     *
     * @param a string containing the year of pubblication
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * Gets the paper id
     *
     * @return a integer containing the paper id
     */
    public int getPaperId() {
        return paperId;
    }

    /**
     * Sets the paperId
     *
     * @param paperId a integer containing the paper id
     */
    public void setPaperId(int paperId) {
        this.paperId = paperId;
    }
}
